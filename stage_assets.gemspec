# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "stage_assets/version"

Gem::Specification.new do |s|
  s.name        = "stage_assets"
  s.version     = StageAssets::VERSION
  s.authors     = ["Brett McHargue"]
  s.email       = ["brett@thestage.co.uk"]
  s.homepage    = ""
  s.summary     = %q{A gem for sharing Stage website branding and layout assets}
  s.description = %q{Common images, javascript libraries and CSS files should be stored in this gem, which should then be added to The Stage's apps.

Assets which are specific to a single application should continue to be stored within the app's asset management structure.}

  s.rubyforge_project = ""

  s.files         = `git ls-files`.split("\n")
  s.require_paths = ["lib"]

  s.add_dependency('bourbon')
  s.add_dependency('neat')
  s.add_dependency('sass', '~> 3.2')
  s.add_dependency('sass-rails')
end
