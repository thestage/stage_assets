module StageAssets::SassExtensions::Functions
end

require 'stage_assets/sass_extensions/functions/constants'
require 'stage_assets/sass_extensions/functions/lists'

module Sass::Script::Functions
  include StageAssets::SassExtensions::Functions::Constants
  include StageAssets::SassExtensions::Functions::Lists
end

class Sass::Script::Functions::EvaluationContext
  include Sass::Script::Functions
end
