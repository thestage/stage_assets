module StageAssets
  require 'stage_assets/version'
  require 'stage_assets/engine'

  require 'stage_assets/sass_extensions'

  SITE_URL = "http://www.thestage.co.uk/example"
end
