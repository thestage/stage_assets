# DO NOT PLACE FILES IN THIS FOLDER

Instead, follow the relevant protocol:

1. Create a partial in the relevant subfolder of `stylesheets/stage_assets/` as appropriate.
2. Add an `@import "<subfolder>/<partial>";` call in the `_<subfolder>.css.scss` file in the parent folder.
