module NavigationHelper
  def nav_tab(title, url, options = {})
    content_tag(:li, link_to(t(title, scope: translate_nav).html_safe, url), options) if enabled?(title)
  end

  def home_tab(options = {})
    content_tag(:li, link_to(t(:home, scope: translate_nav).html_safe, '/', :style => "color: #000;"), options)
  end

  def call_to_action_tab(title, url, options = {})
    content_tag(:li, link_to(t(title, scope: translate_nav).html_safe, url, :style => "color: #fff;"), options.merge(style: 'background-color: #f2871f'))
  end

  def nav_dropdown(title, url, options = {}, &block)
    sub_menu = capture(&block)
    dropdown = link_to (t(title, scope: translate_nav) + "<b class='caret'></b>").html_safe, url, options
    content_tag(:li, class: 'drop') do
       dropdown + content_tag(:ul, sub_menu)
    end
  end

  def sign_up(&block)
    sub_menu = capture(&block)
    dropdown = link_to (t(:sign_up, scope: translate_nav) + "<b class='caret'></b>").html_safe, "#", :style => "color: #fff;"
    content_tag(:li, class: 'drop', style:  'background-color: #f2871f') do
       dropdown + content_tag(:ul, sub_menu)
    end
  end

  def castings_navigation_as_blog_guest
    render partial: "stage_assets/castings/blog_guest"
  end

  private

  def enabled?(title) # default true
    enabled = title.to_s + "_enabled"
    begin
      return Castings.config.send(enabled)
    rescue
      return true
    end
  end

  def translate_nav
    :navigation
  end

end
