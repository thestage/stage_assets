module StageAssets::ApplicationHelper
  def site_title
    t(:site_title) rescue "The Stage Example"
  end
  
  def site_url
    t(:site_url) rescue "http://www.thestage.co.uk/example"
  end
  
  def site_short_name
    if t(:site_short_name).present?
      t(:site_short_name)
    else 
      "Example"
    end
  end

  def link_to_menu_item(text, optional_text = nil, options = {})
    display_text = optional_text || text.humanize
    content_tag :li, :class => text, 'data-name' => text do
      link_to display_text, "http://www.thestage.co.uk/#{text}/", options
    end
  end

  def thank(thank=flash[:thank])
    render '/thank', thank_to: thank[:to], reason: t(thank[:reason]) rescue nil
  end

end
