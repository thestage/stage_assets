module StageAssets::AdvertsHelper
  DFP_TAGS = {
    topleft: {
      width: 215,
      height: 90,
      dom_id: 'div-gpt-ad-1382521414376-0',
      slot_name: 'StageCastings_TopLeft_215x90'
    },
    top: {
      width: 728,
      height: 90,
      dom_id: 'div-gpt-ad-1323370735818-2',
      slot_name: 'Stage_Top_728x90'
    },
    bottomright: {
      width: 300,
      height: 250,
      dom_id: 'div-gpt-ad-1323370735818-1',
      slot_name: 'Stage_BTF_Right_300x250'
    },
    topright: {
      width: 300,
      height: 250,
      dom_id: 'div-gpt-ad-1323370735818-0',
      slot_name: 'Stage_ATF_Right_300x250'
    }
  }
  DFP_SECTION = 'example'  #TODO this needs to be an application specific variable, I assume

  def insert_dfp_tag(slot_type)
    tag_settings = DFP_TAGS[slot_type]

    content_for(:dfp_header) do
      "googletag.defineSlot('/1010711/#{tag_settings[:slot_name]}', [#{tag_settings[:width]}, #{tag_settings[:height]}], '#{tag_settings[:dom_id]}').addService(googletag.pubads()).setTargeting('section','#{DFP_SECTION}');".html_safe
    end

    content_tag(:div, :id => tag_settings[:dom_id], :style => "width: #{tag_settings[:width]}px; height: #{tag_settings[:height]}px;") do
      content_tag(:script, "googletag.cmd.push(function() { googletag.display('#{tag_settings[:dom_id]}'); });".html_safe, :type => 'text/javascript')
    end
  end

  def insert_dfp_header
    if content_for?(:dfp_header)
      html_output = <<-EOSCRIPT
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') +
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
googletag.cmd.push(function() {
#{content_for(:dfp_header)}
googletag.enableServices();
});
</script>
      EOSCRIPT

      html_output.html_safe
    end
  end

  # def impressions(advert)
  #   content_tag(:span) do
  #     impress(advert.impressions_as_premium,"P") + impress(advert.impressions_as_featured, "F") + impress(advert.impressions_as_standard, "S") + "\n" + impress(advert.click_thru_email, "E") + impress(advert.click_thru_website, "W")
  #   end
  # end
  #
  # def impress(count, label)
  #   if count > 0
  #     count.to_s + " [#{label}]"
  #   else
  #     ""
  #   end
  # end
end
