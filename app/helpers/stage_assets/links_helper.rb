module StageAssets::LinksHelper

  def link_to_global_nav(text, optional_text = nil, options = {})
    display_text = optional_text || text.humanize
    content_tag :li, :class => text, 'data-name' => text do
      link_to display_text, "http://www.thestage.co.uk/#{text}/", options
    end
  end
end
