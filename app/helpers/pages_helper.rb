module PagesHelper
  def link_to_top()
    link_to('Top', '#top', :class => 'faq_link link-to-top', :title => 'Back to top')
  end

  def youtube_embed(youtube_url,width=620,height=349)
    if youtube_url[/youtu\.be\/([^\?]*)/]
      youtube_id = $1
    else
      youtube_url[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      youtube_id = $5
    end
    "<iframe width=\"#{width}\" height=\"#{height}\" src=\"http://www.youtube.com/embed/#{ youtube_id }\" frameborder=\"0\" allowfullscreen></iframe>".html_safe
  end
end
